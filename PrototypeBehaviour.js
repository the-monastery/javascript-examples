'use strict';

var arr = ['red', 'blue', 'green'];

Object.defineProperty(Array.prototype, 'last', {get:function(){
  return this[this.length-1];
}});

var last = arr.last;
log(last);

logn("All functions have prototypes");
var myFunc = function() {}
log(myFunc.prototype);

logn("regular objects do not have prototypes");
var cat = {name: 'Fluffy'};
log(cat.prototype)

logn("they do however have a __proto__");
log(cat.__proto__);

function Cat(name, color) {
  this.name = name;
  this.color = color;
}

var fluffy = new Cat('Fluffy', 'White');
var muffin = new Cat('Muffin', 'Brown');

logn("The prototypes are exactly the same");
log(Cat.prototype);
log(fluffy.__proto__);
log(fluffy.__proto__ === Cat.prototype);

logn("You can define a property on the prototype all instances will have it");
Cat.prototype.age = 3;
log(fluffy.age);
log(muffin.age);

logn("You can set a different age for muffin");
muffin.age = 5;
log(fluffy.age);
log(muffin.age);

logn("The original value defined on the prototype however, is still accessible");
log(muffin.__proto__.age);

logn("fluffy doesn't have it's own property called 'age'.. it's on the prototype");
log(Object.keys(fluffy));
log(fluffy.hasOwnProperty('age'));

logn("unless you define it on the instance of fluffy");
fluffy.age = 3;
log(Object.keys(fluffy));
log(fluffy.hasOwnProperty('age'));

logn("You can point the prototype to a different object");
Cat.prototype = {age:666};

logn("but it won't have an affect on exiting objects");
log(fluffy.age);
log(muffin.age);

logn("It will however effect new ones");
var snowball = new Cat('snowball', 'white');
log(snowball.age);

//This is because the original prototype was created in memory, still exists,
//and both Fluffy and Muffin are pointing to that prototype
//The new prototype was created in memory as well, but only new Cat instances of
//Cat will point to that one.
//Prototypes are objects that live in memory and behave like other objects in memory

logn("Prototype inheritance chain.. eventually you will reach null");
log(fluffy.__proto__);
log(fluffy.__proto__.__proto__);
log(fluffy.__proto__.__proto__.__proto__);

function Animal(voice) {
  this.voice = voice || 'grunt';
}
Animal.prototype.speak = function() {
  log(this.voice);
}
function Dog(){
  //This is how you chain the constructors");
  Animal.call(this, 'Woof');
}
logn("Setting the prototype using Object.create prevents the calling of the constructor");
Dog.prototype = Object.create(Animal.prototype);

var spot = new Dog();
spot.speak();

logn("this shows that the constructor is not set correctly (spot is a Dog not an Animal)");
log(spot);

logn("this shows that spot is indeed both and Animal and a Dog");
log(spot instanceof Dog);
log(spot instanceof Animal);

logn("This is how you get the correct constructor");
Dog.prototype.constructor = Dog;
log(spot);

logn("Let's look at the chain again");
log(spot.__proto__);
log(spot.__proto__.__proto__);
log(spot.__proto__.__proto__.__proto__);
log(spot.__proto__.__proto__.__proto__.__proto__);

function log(output) {console.log(output);}
function logn(output) {console.log("\n-- " + output + " --");}