'use strict';

class Animal {
  constructor(voice) {
    this.voice = voice || 'grunt';
  }

  speak() {
    console.log(this.voice);
  }
}

class Dog extends Animal {
  constructor(name, color) {
    super('Woof');
    this.name = name;
    this.color = color;
  }
}

var spot = new Dog('Spot', 'Brown');
spot.speak();

console.log(spot.constructor);
console.log(Object.keys(spot));
console.log(Object.keys(spot.__proto__));
console.log(spot.__proto__.hasOwnProperty('name'));
console.log(spot.__proto__.__proto__.hasOwnProperty('speak'));