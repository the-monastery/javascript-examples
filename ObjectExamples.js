function Cat(color, fullName) {

  this.name = {first:"", last:""};
  this.color = color;
  this.fullName = fullName;

  this.speak = function() {
    console.log("Meow!")
  };

  Object.defineProperty(this, "fullName", {
    get: function() {
      return this.name.first + " " + this.name.last;
    },
    set: function(input) {
      var split = input.split(" ");
      this.name.first = split[0];
      this.name.last = split[1];
    }
  });

  Object.defineProperty(this, "color", {
    value: "Red",
    enumerable: false,
    configurable: false,
    writable: false
  });
}

var cat = new Cat("White", "Fluffy LaBoef");
console.log(cat.color);
console.log(cat.fullName);
console.fullName = "Bob Cagen";
console.log(cat.name.first);
console.log(cat.name.last);
console.log(cat.fullName);
console.log(cat.speak());
console.log(Object.getOwnPropertyDescriptor(cat, "color"));
console.log(Object.getOwnPropertyDescriptor(cat, "first"));
console.log(Object.getOwnPropertyDescriptor(cat, "last"));
console.log(Object.getOwnPropertyDescriptor(cat, "fullName"));
console.log(Object.getOwnPropertyNames(cat));
console.log(Object.keys(cat));